import pandas as pd
import numpy as np
from os import listdir
from os.path import isfile, join
import csv
from Fila import FilaDePrioridade as fila

def genPriority():

    priority={}

    for f in listdir("csv/"):
        df = pd.read_csv("csv/" + f)
        priority[f] = []
        
        for index, linha in df.iterrows():
            if linha.isnull().values.any() == True:
                continue
            data = ['']*7
            data[0] = linha["timestamp"]
            data[1] = linha["umid_max"]
            data[2] = linha["umid_min"]
            data[3] = linha["temp_max"]
            data[4] = linha["temp_min"]
            data[5] = linha["latitude"]
            data[6] = linha["longitude"]
            priority[f].append(data)
    return priority