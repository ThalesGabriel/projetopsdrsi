1. Inicie o thingsboard

2. Importe o dashboard da pasta "Dashboard"

3. Crie todos os devices referentes as cidades

4. Cada cidade é representada no csv na 3° coluna
    
    - Crie o device no padrão "Info{{NomeDaCidade}}"<space><space>

5. Crie todos os assets referentes as cidades
    
    - Cada cidade é representada no csv na 3° coluna
    
    - Cada asset de cidade tem o asset type definido como "city"
    
    - Cada asset tem os atributos latitude e longitude que podem ser encontrados na 4° e 5° colunas de cada csv
    
    - Defina a relação desse asset com o seu device
        
        - Exemplo: Asset Recife Contains device InfoRecife
        
6. Ligue o kafka-producer, consumer e o sparkequation

7. Deixe rodando um tempo

8. Entre no dashboard que você importou e clique num marcador de cidade, o próximo dashboard exibirá as informações necessárias
    
    