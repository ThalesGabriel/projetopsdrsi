"""
 Consumes messages from one or more topics in Kafka and does an equation.
 Usage: spark_equation.py <bootstrap-servers> <subscribe-type> <topics>
   <bootstrap-servers> The Kafka "bootstrap.servers" configuration. A
   comma-separated list of host:port.
   <subscribe-type> There are three kinds of type, i.e. 'assign', 'subscribe',
   'subscribePattern'.
   |- <assign> Specific TopicPartitions to consume. Json string
   |  {"topicA":[0,1],"topicB":[2,4]}.
   |- <subscribe> The topic list to subscribe. A comma-separated list of
   |  topics.
   |- <subscribePattern> The pattern used to subscribe to topic(s).
   |  Java regex string.
   |- Only one of "assign, "subscribe" or "subscribePattern" options can be
   |  specified for Kafka source.
   <topics> Different value format depends on the value of 'subscribe-type'.

 Run the example
    `$  \
    host1:port1,host2:port2 subscribe topic1,topic2`
    
    bin/spark-submit --packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.4.3 /home/rsi-psd-vm/Documents/rsi-psd-codes/psd/2019-2/SEILA/sparkequation.py localhost:9092 subscribe fila
"""
from __future__ import print_function

import sys

import requests as req 

from pyspark.sql import SparkSession
from pyspark.sql.functions import explode
from pyspark.sql.functions import split
from pyspark.sql.functions import from_json
from pyspark.sql.types import *

def indiceDeCalor(temperatura, umidade):
    temp = converteFahrenheit(float(temperatura))
    umidadePorcentagem = float(umidade)
    indiceCalor = str(((1.1*temp) + (0.047 * umidadePorcentagem)) - 10.3)
    indiceCalor = float(indiceCalor[0:5])
    if(indiceCalor < 80.0):
        return respostaPratica(indiceCalor)
    return respostaPratica(indiceAlarmante(temp,umidadePorcentagem))

def indiceAlarmante(T, RH):
    indiceCalor = -42.379 + 2.04901523*T + 10.14333127*RH - 0.22475541*T*RH - 0.00683783*T*T - 0.05481717*RH*RH + 0.00122874*T*T*RH + 0.00085282*T*RH*RH - 0.00000199*T*T*RH*RH
    if((T >= 80 or T <= 112) and RH <= 13):
        return indiceAlarmante2(T, RH, indiceCalor)
    elif((T >= 80 or T <= 87) and RH > 85):
        return indiceAlarmante3(T, RH, indiceCalor)
    return indiceCalor

def converteFahrenheit(temperatura):
    return (1.8*temperatura) + 32

def converteCelsius(HI):
    return ((HI - 32) / 1.8)

def indiceAlarmante2(T, RH, HI):
    variavel = T - 95
    if(variavel < 0):
        variavel *= -1
    variavel2 = 17 - variavel
    if(variavel2 < 0):
        variavel2 *= -1
    indiceCalor = HI - (3.25 - 0.25 * RH) * ((variavel2/17)**0.5)
    return indiceCalor

def indiceAlarmante3(T, RH, HI):
    indiceCalor = HI + 0.02 * (RH - 85) * (87-T)
    return indiceCalor

def respostaPratica(HI):
    HI = float(str(HI)[0:4])
    HI = converteCelsius(HI)
    situation = ""
    if(HI <= 27):
        situation = "Normal"
    elif(HI > 27 and HI <= 32):
        situation = "Be aware"
    elif(HI > 32 and HI <= 41):
        situation = "Warning"
    elif(HI > 41 and HI <= 54):
        situation = "Danger"
    elif(HI > 54):
        situation = "Extreme Danger"
    return [HI, situation]



def sendToThingsboard(x, indice):
    latitude = str(x["latitude"])[0:10]
    longitude = str(x["longitude"])[0:10]
    accessToken = ""
    if float(latitude) == -8.504 and float(longitude) == -39.31528:
        accessToken = "1FDNmlnRBRCRxDzJfqeL" #Cabrobo

    elif float(latitude) == -8.05928 and float(longitude) == -34.959239:
        accessToken = "tscaoo687SD1oye3hb6w" #Recife

    elif float(latitude) == -9.388323 and float(longitude) == -40.523262:
        accessToken = "jkmiNza7tXhzcAk0ZkFo" #Petrolina

    elif float(latitude) == -8.433544 and float(longitude) == -37.055477:
        accessToken = "DPTwZvdOqsD5lMkuDvLf" #Arco Verde

    elif float(latitude) == -8.910950 and float(longitude) == -36.493381:
        accessToken = "0psJUyMlolUMVHbI43cK" #Garanhuns

    elif float(latitude) == -7.839628 and float(longitude) == -35.801056:
        accessToken = "ejP14Ud0og5gdXAW5JTN" #Surubim

    elif float(latitude) == -8.236069 and float(longitude) == -35.985550:
        accessToken = "2SMMEgT1PYHZRAWGNGdE" #Caruaru

    elif float(latitude) == -8.509552 and float(longitude) == -37.711591:
        accessToken = "8bSzG9wHb9f795hnDaZQ" #Ibimirim

    elif float(latitude) == -7.954277 and float(longitude) == -38.295082:
        accessToken = "o9fknVgiEFMVDgz55XQB" #Serra Talhada

    elif float(latitude) == -8.598785 and float(longitude) == -38.584062:
        accessToken = "dZP9JNvfPHXBEOwsor0G" #Floresta

    elif float(latitude) == -8.666667 and float(longitude) == -35.567921:
        accessToken = "olUBy2GLXo07iHkTCEwy" #Palmares

    elif float(latitude) == -7.885833 and float(longitude) == -40.102683:
        accessToken = "ofxpGfUgMmYye3yGpTcr" #Ouricuri

    else:
        accessToken = "NFWIsSV6lT6nTWDIy8o2" #Salgueiro

    values = ( "{\"ts\": %s, \"values\":{ \"HI\": %s, \"status\": %s }}" %(x[4], float(str(indice[0])[0:5]), indice[1]))
    req.post('http://localhost:9090/api/v1/%s/telemetry' %(accessToken), data=values)
    print(values)

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("""
        Usage: spark_equation.py <bootstrap-servers> <subscribe-type> <topics>
        """, file=sys.stderr)
        sys.exit(-1)
    bootstrapServers = sys.argv[1]
    subscribeType = sys.argv[2]
    topics = sys.argv[3]

    # client1= client.Client("control1")                    #create client object
    # client1.username_pw_set("MTygNSy6zeuUTBiECYw4")               #access token from thingsboard device
    # client1.connect("localhost",1883,keepalive=60000)

    spark = SparkSession\
        .builder\
        .appName("SparkEquation")\
        .getOrCreate()

    
    df = spark.readStream\
        .format("kafka")\
        .option("kafka.bootstrap.servers", "localhost:9092")\
        .option("subscribe", "fila")\
        .option("startingOffsets", "latest")\
        .load()
    
    personJsonDf = df.selectExpr("CAST(value AS STRING)")

    # ola = personJsonDf.foreach(printEach)

    struct = StructType([
        StructField("ts", StringType(), True),
        StructField("umidade_maxima", StringType(), True),
        StructField("temperatura_maxima", StringType(), True),
        StructField("temperatura_minima", StringType(), True),
        StructField("umidade_minima", StringType(), True),
        StructField("latitude", StringType(), True),
        StructField("longitude", StringType(), True)
    ])

    personNestedDf = personJsonDf.select(from_json("value", struct).alias("values"))

    # personNestedDf.printSchema()
    
    personFlattenedDf = personNestedDf.selectExpr("values.temperatura_maxima", "values.temperatura_minima", "values.umidade_maxima", "values.umidade_minima", "values.ts", "values.latitude", "values.longitude")

    def printEach(x):
        indice = indiceDeCalor(x[0], x[2])
        sendToThingsboard(x, indice)
        # print(x[0], x[1], x[2], x[3])
        # print(heat_index_fahrenheit_alg(celsius_to_fahrenheit(float(x[0])), float(x[2])))

    consoleOutput = personFlattenedDf\
        .writeStream\
        .foreach(printEach)\
        .start()
        # .outputMode("append")\
        # .format("console")\

     
    
    consoleOutput.awaitTermination()