from kafka import KafkaConsumer
from json import loads
from paho.mqtt import client
import requests as req 
from paho.mqtt import publish

consumer = KafkaConsumer(
    'fila',
     bootstrap_servers=['localhost:9092'],
     auto_offset_reset='latest',
     enable_auto_commit=True,
     group_id='my-group',
     value_deserializer=lambda v: v.decode('utf-8')
            .replace("(", "")
            .replace(")", "")
            .replace("[", "")
            .replace("]", "")
            .replace(" ", "")
     ) 



def sendToThingsboard(x):
    latitude = str(x["latitude"])[0:10]
    longitude = str(x["longitude"])[0:10]
    accessToken = ""
    if float(latitude) == -8.504 and float(longitude) == -39.31528:
        accessToken = "SrJTTEMUq10TXoIPHAs5" #Cabrobo

    elif float(latitude) == -8.05928 and float(longitude) == -34.959239:
        accessToken = "rAVsGK2C4fWPGlBWAWD9" #Recife

    elif float(latitude) == -9.388323 and float(longitude) == -40.523262:
        accessToken = "96cCgKAknGTX2JXdK6iH" #Petrolina

    elif float(latitude) == -8.433544 and float(longitude) == -37.055477:
        accessToken = "6OJyfktjla86nWTMz05D" #Arco Verde

    elif float(latitude) == -8.910950 and float(longitude) == -36.493381:
        accessToken = "XiE6u4NvdYsiLPCkQVSp" #Garanhuns

    elif float(latitude) == -7.839628 and float(longitude) == -35.801056:
        accessToken = "HAXIcGnqduYaWOM8vJcK" #Surubim

    elif float(latitude) == -8.236069 and float(longitude) == -35.985550:
        accessToken = "F40zqiItdLKHqfuXaQzl" #Caruaru

    elif float(latitude) == -8.509552 and float(longitude) == -37.711591:
        accessToken = "2cdDcB9ExliBk18v2W7v" #Ibimirim

    elif float(latitude) == -7.954277 and float(longitude) == -38.295082:
        accessToken = "HAXIcGnqduYaWOM8vJcK" #Serra Talhada

    elif float(latitude) == -8.598785 and float(longitude) == -38.584062:
        accessToken = "9nvjwTOnCjKGVyFHDO8c" #Floresta

    elif float(latitude) == -8.666667 and float(longitude) == -35.567921:
        accessToken = "UxljJgIhZKFPVA7zj6QZ" #Palmares

    elif float(latitude) == -7.885833 and float(longitude) == -40.102683:
        accessToken = "KSeIBoldObxA8bE0XxvX" #Ouricuri

    else:
        accessToken = "xxd1nQlraIRvJc8OblsR" #Salgueiro
    values = ( "{\"ts\": %s, \"values\":{ \"umidade_maxima\": %s, \"temperatura_maxima\": %s, \"temperatura_minima\": %s, \"umidade_minima\": %s }}" %(y["ts"],y["umidade_maxima"], y["temperatura_maxima"], y["temperatura_minima"] ,y["umidade_minima"]) )
    req.post('http://localhost:9090/api/v1/%s/telemetry' %(accessToken), data=values)
    print(accessToken)
                 #create client object

for message in consumer:
    message = message.value
    y = loads(message)
    sendToThingsboard(y)