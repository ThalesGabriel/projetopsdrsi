from kafka import KafkaProducer
import json
from time import sleep
from datetime import datetime
import lorem
import time
from Fila import FilaDePrioridade as fila
from event_generator import genPriority
from projeto import *

prioritys = genPriority()
# Create an instance of the Kafka producer
producer = KafkaProducer(bootstrap_servers='localhost:9092', value_serializer=lambda v: str(v).encode('utf-8'))

# Call the producer.send method with a producer-record

for i in range(len(prioritys['A307.csv'])):
    for priority in prioritys:
        info = prioritys[priority][i]
        print(info)
        valores = ( "{\"ts\": %s, \"umidade_maxima\": %s, \"temperatura_maxima\": %s, \"temperatura_minima\": %s, \"umidade_minima\": %s, \"latitude\": %s, \"longitude\": %s }" %(info[0], info[1], info[3], info[4], info[2], info[5], info[6]) )
        print(valores)
        # print("Índices máximos:" + indiceDeCalor(priority[1][2], priority[1][0]))
        # print("Índices minimos:" + indiceDeCalor(priority[1][3], priority[1][1]))
        producer.send('fila', valores)
        time.sleep(3)